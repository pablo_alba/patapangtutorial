extends KinematicBody2D

signal player_hit_signal

const GRAVITY = 10
const BOUNCE_VEL = -14
var motion = Vector2(5, 0)
var paused = false


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	

func pause():
	paused = true

func _physics_process(delta):
	if ! paused:		
		if position.x < 0 or position.x + (256 * scale.x) > 1920:
			motion.x = - motion.x
		
		motion.y += delta * GRAVITY
		var collision = move_and_collide(motion)
		if (collision != null):
			if ("Floor" == collision.collider.name):
				motion.y = BOUNCE_VEL
			elif ("Player" == collision.collider.name):
				emit_signal("player_hit_signal")
	
