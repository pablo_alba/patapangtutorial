extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	update_music()
	update_fx()



func _on_PlayButton_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		get_tree().change_scene("res://Play.tscn")


func _on_MusicButton_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		Globals.music_enabled = false
		update_music()

func _on_MusicButtonOff_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		Globals.music_enabled = true
		update_music()

func _on_FxButton_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:		
		Globals.fx_enabled = false
		update_fx()

func _on_FxButtonOff_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		Globals.fx_enabled = true
		update_fx()
		
func update_music():
	if Globals.music_enabled:
		$AudioStreamPlayer.play()
		$MusicButtonOff.hide()
		$MusicButton.show()
	else:
		$AudioStreamPlayer.stop()
		$MusicButtonOff.show()
		$MusicButton.hide()
		
func update_fx():
	if Globals.fx_enabled:
		$AudioStreamPop.play()
		$FxButtonOff.hide()
		$FxButton.show()
	else:
		$FxButtonOff.show()
		$FxButton.hide()
		
