extends Area2D

signal arrow_touch_ceil_signal
signal arrow_hit_signal

var motion = Vector2(0, -1000)

func _ready():
	pass # Replace with function body.


func _physics_process(delta):
	translate(motion * delta)
	if position.y < 0:
		emit_signal("arrow_touch_ceil_signal")
	else:
		var bodies = get_overlapping_bodies()
		for body in bodies:
			if "Ball" in body.name:
				emit_signal("arrow_hit_signal", body)
