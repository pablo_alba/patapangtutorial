extends KinematicBody2D
signal player_fire_signal

const SPEED = 500
var motion = Vector2()
var paused = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	
func idle():
	paused = false
	$AnimatedSprite.animation = "adventure_guy_idle"
	
func die():
	paused = true
	$AnimatedSprite.animation = "adventure_guy_dead"
	
func _physics_process(delta):
	motion.y = 0
	if ! paused:
		if Input.is_action_pressed("ui_accept"):
			emit_signal("player_fire_signal")
		elif Input.is_action_pressed("ui_right"):
			motion.x = SPEED
			$AnimatedSprite.flip_h = false		
			$AnimatedSprite.animation = "adventure_guy_running"
		elif Input.is_action_pressed("ui_left"):
			motion.x = -SPEED
			$AnimatedSprite.flip_h = true
			$AnimatedSprite.animation = "adventure_guy_running"
		else:
			$AnimatedSprite.animation = "adventure_guy_idle"
			motion.x = 0
			
		motion = move_and_slide(motion)
		if position.x < 0:
			position.x = 0
		elif position.x > 1750:
			position.x = 1750
