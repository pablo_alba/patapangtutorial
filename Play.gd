extends Node2D
var arrow
var balls

# Called when the node enters the scene tree for the first time.
func _ready():
	if Globals.music_enabled:
		$AudioStreamMusic.play()
	randomize()
	balls = []	
	$Player.connect("player_fire_signal", self, "player_fire")
	reset()

func create_ball(position, motion, scale):
	var ball = load("res://Ball.tscn").instance()
	ball.position.x = position.x
	ball.position.y = position.y
	ball.motion.x = motion.x
	ball.motion.y = motion.y
	ball.scale.x = scale
	ball.scale.y = scale
	add_child(ball)
	ball.connect("player_hit_signal", self, "player_hit")
	balls.append(ball)

func player_hit():
	for ball in balls:
		ball.pause()
	$Player.die()
	yield(get_tree().create_timer(2.0), "timeout")
	show_gameover()
	
func player_fire():
	if arrow == null:
		arrow = load("res://Arrow.tscn").instance()
		arrow.position.x = $Player.position.x + 75
		arrow.position.y = $Floor.position.y
		arrow.connect("arrow_touch_ceil_signal", self, "arrow_touch_ceil")
		arrow.connect("arrow_hit_signal", self, "arrow_hit")
		add_child(arrow)

func arrow_touch_ceil():
	remove_child(arrow)
	arrow = null
	
func arrow_hit(ball):
	if arrow != null:
		if Globals.fx_enabled:
			$AudioStreamPop.play()
		balls.erase(ball)
		remove_child(ball)
		remove_child(arrow)
		arrow = null
		if ball.scale.x > 0.25:
			var scale = ball.scale.x / 2
			var motion1 = Vector2(ball.motion.x, -abs(ball.motion.y))
			var motion2 = Vector2(-ball.motion.x, -abs(ball.motion.y))
			create_ball(ball.position, motion1, scale)
			create_ball(ball.position, motion2, scale)
		if balls.size() == 0:
			yield(get_tree().create_timer(0.5), "timeout")
			show_gameover()


func _on_Home_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		get_tree().change_scene("res://MainMenu.tscn")


func _on_Reload_gui_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		reset()
		
		
func show_gameover():
	if (arrow != null):
		remove_child(arrow)
		arrow = null
		
	for ball in balls:
		remove_child(ball)
	balls.clear()
	$Gameover.show()
		
func reset():
	$Player.position.x = 845
	$Player.idle()
	
	var num_balls = randi()%3+1
	var size = 1
	if num_balls == 3:
		size = 0.5
	for ball in range(num_balls):
		var x = randi()%1500 + 200
		var dir = 1
		if randi() % 2:
			dir = -1
		create_ball(Vector2(x,0), Vector2(5 * dir, 0), size)
	
	$Gameover.hide()
		
